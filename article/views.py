from django.http import JsonResponse, HttpResponse
from django.shortcuts import render, get_object_or_404
from django.views import  generic

# Create your views here.
from article.models import Article, Category, Comment
from django.views.decorators.csrf import csrf_exempt
from rest_framework import viewsets, status
from rest_framework.decorators import api_view, action
from article.serializers import ArticleSerializer, CategorySerializer,CommentSerializer
from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from rest_framework.views import APIView



def index(request):

    num_articles= Article.objects.all().count()
    num_categories= Category.objects.all().count()
    articles = Article.objects.filter(category__name__contains='actu')

    context = {
        'num_articles' : num_articles,
        'num_categories': num_categories,
        'articles_list' : articles
    }

    return render(request, 'index.html', context=context)




@api_view(['GET','POST'])
def category_index(request):

    if request.method == 'GET':
        categories = Category.objects.all()
        serializer = CategorySerializer(categories, many=True)
        return Response(serializer.data)

    if request.method == 'POST':
        data = request.data
        serializer = CategorySerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET','POST'])
def comment_list(request):

    if request.method == 'GET':
        comments = Comment.objects.all()
        serializer = CommentSerializer(comments, many=True)
        data = serializer.data
        return Response(data)

    if request.method == 'POST':
        data = request.data
        serializer = CommentSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET','POST'])
def comment_list_by_article(request,pk):

    if request.method == 'GET':
        comments = Comment.objects.filter(article__pk=pk)
        serializer = CommentSerializer(comments, many=True)
        return Response(serializer.data)

    if request.method == 'POST':
        data = request.data
        serializer = CommentSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



class ArticlesList(APIView):
    def get(self,request):
        articles = Article.objects.all()
        serializer = ArticleSerializer(articles,many=True)
        return Response(serializer.data)

    def post(self, request):
        data = request.data
        serializer = CategorySerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET','PUT','DELETE'])
def comment_detail(request,pk):

    try:
        comment = Comment.objects.get(pk=pk)
    except Comment.DoesNotExist:
        return Response({"error":f"le commentaire avec l'id {pk} n'existe pas"}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = CommentSerializer(comment)
        data = serializer.data
        return Response(data, status=status.HTTP_200_OK)

    if request.method =='PUT':
        data = request.data
        serializer = CommentSerializer(comment, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    if request.method == 'DELETE':
        comment.delete()
        return Response({"response":f"le commentaire avec l'id {pk} a été supprimé"},status=status.HTTP_204_NO_CONTENT)






@api_view(['GET','PUT','DELETE'])
def category_detail(request,pk):

    try:
        category = Category.objects.get(pk=pk)
    except Category.DoesNotExist:
        return Response({"error":f"la catégorie avec l'id {pk} n'existe pas"}, status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = CategorySerializer(category)

        return Response(serializer.data, status=status.HTTP_200_OK)

    if request.method =='PUT':
        data = request.data
        serializer = CategorySerializer(category, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    if request.method == 'DELETE':
        if category.name == 'Opinion':
            return Response({"error":"Vous ne pouvez pas supprimer une opinion"},status=status.HTTP_401_UNAUTHORIZED)
        else:
            category.delete()
            return Response({"response":f"la catégorie avec l'id {pk} a été supprimée"},status=status.HTTP_204_NO_CONTENT)










@api_view(['GET'])
def articles_api(request):
    if request.method == 'GET':
        articles = Article.objects.all()
        serializer = ArticleSerializer(articles, many = True)
        return Response(serializer.data)



def comments_index(request):
    if request.method == 'GET':
        articles = Article.objects.all()
        serializer = ArticleSerializer(articles, many=True)
        return Response(serializer.data)


class ArticleListView(generic.ListView):
    model = Article
    queryset = Article.objects.all()


def article_detail(request, slug):
    article = Article.objects.get(slug=slug)
    context = {
        'article': article
    }
    return render(request, 'article_detail.html', context=context)


#2eme etape
class ArticleViewSet(viewsets.ModelViewSet):
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer



class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class ArticleDetailView(generic.DetailView):
    model = Article
    template_name = 'article_detail.html'


class CategoryListView(generic.ListView):
    model = Category
    context_object_name = 'categories'

    def get_queryset(self):
        return Category.objects.filter(name__contains=self.kwargs['name'])

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['test'] = 'un test en texte'
        context['number'] = 53
        return context

    #contect = category_list
