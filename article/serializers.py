from rest_framework import serializers
from .models import Article, Category, Comment

#Premiere etape
class ArticleSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Article
        fields = ('id', 'title', 'summary', 'content','category','cover_image')

class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = ('id','author_name', 'content', 'created_at', 'article')
        depth=1




class CategorySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Category
        fields = ('id','name')



