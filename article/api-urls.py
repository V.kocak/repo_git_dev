from django.urls import path, include
from rest_framework import routers

from . import views

#creer router
router = routers.DefaultRouter()
router.register('articles', views.ArticleViewSet)
router.register('categories', views.CategoryViewSet)

urlpatterns = [
path('', include(router.urls), name='api'),
path('comments/', views.comment_list),
path('comments/<int:pk>/', views.comment_detail),
path('articles/<int:pk>/comments', views.comment_list_by_article)

]